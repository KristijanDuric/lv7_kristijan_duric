﻿using System;

namespace RazvojLV7
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] Array_to_sort = { 5, 12, 8, 9, 1, 2, 45, 24, 22, 40 };
            NumberSequence Array = new NumberSequence(Array_to_sort);
            BubbleSort SortMetode = new BubbleSort();
            Array.SetSortStrategy(SortMetode);
            Array.Sort();
            Console.WriteLine(Array.ToString());

            NumberSequence Array_two = new NumberSequence(Array_to_sort);
            CombSort SortMetode_two = new CombSort();
            Array_two.SetSortStrategy(SortMetode_two);
            Array_two.Sort();
            Console.WriteLine(Array_two.ToString());

            NumberSequence Array_three = new NumberSequence(Array_to_sort);
            SequentialSort SortMetode_three = new SequentialSort();
            Array_three.SetSortStrategy(SortMetode_three);
            Array_three.Sort();
            Console.WriteLine(Array_three.ToString());

        }

    }
}

﻿using System;

namespace RazvojLV7
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberSequence sequence = new NumberSequence(10);
            SortStrategy strategy = new BubbleSort();
            Random generator = new Random();
            for (int i = 0; i < 10; i++)
            {
                sequence.InsertAt(i, generator.Next(1, 10));
            }
            sequence.SetSortStrategy(strategy);
            sequence.Sort();
            Console.WriteLine(sequence.ToString());

            Searching search = new LinearSearch(1);
            sequence.setsearchstrategy(search);
            sequence.Search();

        }

    }
}

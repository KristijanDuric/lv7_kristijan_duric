﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RazvojLV7
{
    class LinearSearch : Searching
    {
        private double x;
        public LinearSearch(double x)
        {
            this.x = x;
        }

        public void Search(double[] array)
        {
            int n = array.Length;
            int f = 0;
            for (int i = 0; i < n; i++)
            {
                if (array[i] == x)
                {
                    f += 1;
                }
            }
            if (f != 0)
            {
                Console.WriteLine("Number is found");
            }
            else
            {
                Console.WriteLine("Number is not found");

            }

        }
    }
}
